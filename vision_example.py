import os, dotenv
from utils.webcam_util import CaptureFromWebcam

dotenv.load_dotenv()

import base64
import requests

# OpenAI API Key
api_key = os.getenv("OPENAI_API_KEY")


def LoadAndEncode(image_path):
  with open(image_path, "rb") as image_file:
    return base64.b64encode(image_file.read()).decode('utf-8')


def DescribeImage(base64_image) -> str:
    headers = {
    "Content-Type": "application/json",
    "Authorization": f"Bearer {api_key}"
    }

    payload = {
    "model": "gpt-4-vision-preview",
    "messages": [
        {
        "role": "user",
        "content": [
            {
                "type": "text",
                "text": "What's in this image?"
            },
            {
                "type": "image_url",
                "image_url": {
                    "url": f"data:image/jpeg;base64,{base64_image}"
                }
            }
        ]
        }
    ],
    "max_tokens": 300
    }

    response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload)

    message_content = response.json()["choices"][0]["message"]["content"]

    return message_content


def GetDescriptionOfImage(display=True):
    base64_image = CaptureFromWebcam(display=display)
    if not base64_image:
        print("Failed to capture image from webcam, using backup image.")
        base64_image = LoadAndEncode("utils/backup_img.png")


    image_description = DescribeImage(base64_image)
    return image_description


if __name__ == "__main__":
    response = GetDescriptionOfImage()

    print("\n====================\n")
    print(response)
    print("\n====================\n")