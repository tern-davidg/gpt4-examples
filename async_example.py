import os, dotenv
import openai
from utils.openai_utils import UserMessage, SystemMessage
import asyncio


dotenv.load_dotenv()

openai_client = openai.AsyncClient()

MODEL_GPT4 = "gpt-4"


async def pirate_example():
    completion = await openai_client.chat.completions.create(
        messages = [
            SystemMessage("Respond in the style of a pirate."),
            UserMessage("Tell me how to use the openAI gpt4 api."),
        ],
        model=MODEL_GPT4,
        max_tokens=100
    )

    return completion.choices[0].message.content


async def streamed_example():
    completion_chunks = await openai_client.chat.completions.create(
        messages = [
            SystemMessage("Respond in the style of a pirate."),
            UserMessage("Tell me how to use the openAI gpt4 api."),
        ],
        model=MODEL_GPT4,
        max_tokens=100,
        stream=True
    )

    async for chunk in completion_chunks:
        chunk_token = chunk.choices[0].delta.content
        print(chunk_token, end='')
    print("")


if __name__ == '__main__':
    async def main():
        # print(await pirate_example())
        await streamed_example()

    asyncio.run(main())