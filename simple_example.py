import os, dotenv
import openai
from utils.openai_utils import UserMessage, SystemMessage

dotenv.load_dotenv()

openai_client = openai.Client()

MODEL_GPT4 = "gpt-4"

def simple_example():
    completion = openai_client.chat.completions.create(
        messages = [
            UserMessage("Give me a recipe for chocolate cake."),
        ],
        model=MODEL_GPT4,
        max_tokens=100,
        temperature=2
    )

    return completion.choices[0].message.content


def pirate_example():
    completion = openai_client.chat.completions.create(
        messages = [
            SystemMessage("Respond in the style of a pirate."),
            UserMessage("Give me a recipe for chocolate cake."),
        ],
        model=MODEL_GPT4,
        max_tokens=100
    )

    return completion.choices[0].message.content


def json_example():
    completion = openai_client.chat.completions.create(
        messages = [
            SystemMessage("""
Parse the user's message into json, in the following format.
                          
{
    "drink": <drink name>,
    "milk": <milk type>,
    "extra requests": <extra requests>
}
""".strip()),

            UserMessage("I'd like a froppamochachino with oat milk and extra foam."),
        ],
        model=MODEL_GPT4,
        max_tokens=100
    )

    return completion.choices[0].message.content


if __name__ == '__main__':
    # response = simple_example()
    # response = pirate_example()
    response = json_example()

    print("\n====================\n")
    print(response)
    print("\n====================\n")