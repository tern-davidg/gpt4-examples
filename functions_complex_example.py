import asyncio
import dotenv
from pydantic import BaseModel
from utils.gpt_tool import GPTTool, TaskResponse, SystemMessage, UserMessage, gpt_function
import json
import random

dotenv.load_dotenv()


class AirportVehicle(BaseModel):
    callsign: str
    current_location: str
    vehicle_type: str
    has_pushback_clearance: bool = False

    def __str__(self):
        return f"{self.callsign} at {self.current_location}"
    

class Airport(BaseModel):
    active_runway: str = "27L"
    taxiways: list[str] = ["Alpha", "Bravo 1", "Bravo 2", "Charlie", "Delta", "Echo", "Foxtrot", "Golf", "Hotel", "Mike", "November", "Oscar", "Papa", "Quebec", "Romeo", "Sierra", "Tango", "Uniform", "Victor", "Whiskey", "X-ray", "Yankee", "Zulu"]
    TEST_path_across_active_runway: bool = False
    TEST_can_enter_runways: bool = False

    def FindPath(self, start: str, end: str):
        if end == self.active_runway:
            end = f"ENTER RUNWAY: {end}"
        if not self.TEST_path_across_active_runway:
            return [*random.choices(self.taxiways, k=5), end]
        else:
            return [*random.choices(self.taxiways, k=2), f"CROSS RUNWAY: 09", *random.choices(self.taxiways, k=2), end]

    def CanEnterRunway(self, runway_id: str):
        return self.TEST_can_enter_runways


class ATCTool(GPTTool):
    def __init__(self, airport: Airport, vehicle: AirportVehicle, model: str = "gpt-4-1106-preview"):
        super().__init__(model=model)
        self.airport = airport
        self.vehicle = vehicle

    @property
    def system_messages(self):
        return [
            SystemMessage(f"""
You are an Air Traffic Controller at Keflavik airport, callsign 'Tower'. You are responsible for ensuring safe and efficient movement of vehicles on the ground and in the air. Be concise and use proper phraseology, communication. You are talking to callsign '{vehicle.callsign}' ({vehicle.vehicle_type}) at {vehicle.current_location}. The active runway is {self.airport.active_runway}.

"Ready to taxi" = "taxi to active runway"

Eg: 
Icelandair 456 reports ready to taxi. Active runway is 18.
1. Call find_path("18") -> ["Mike", "Bravo", "CROSS RUNWAY: 32", "Delta", "Tango", "ENTER RUNWAY: 18"]
2. Call can_enter_runway("32", "Bravo") -> True
3. Call can_enter_runway("18", "Tango") -> False
"Icelandair 456, taxi to runway 18 via Mike, Bravo, Delta, cross runway 32, Tango, report clear of runway 32. Hold at Tango."

Eg:
November 12345 reports ready to taxi. Active runway is 27.
1. Call find_path("27") -> ["Bravo", "CROSS RUNWAY: 18", "Mike", "Charlie", "ENTER RUNWAY: 27"]
2. Call can_enter_runway("18", "Bravo") -> False
"November 12345, taxi to Runway 27 via Taxiway Bravo, Mike, Charlie. Hold short of Runway 18 at Bravo."

Eg:
Snow King requests taxi from service apron to terminal. Active runway is 27.
1. Call find_path("Terminal") -> ["Mike", "Bravo", "CROSS RUNWAY: 27", "Delta", "Tango", "Terminal"]
2. Call can_enter_runway("27", "Bravo") -> False
"Snow King, taxi to terminal via Mike, Bravo, Delta-2, Kilo, hold short of Runway 27 at Bravo."

Eg:
Snow King requests taxi from service apron to terminal. Active runway is 27.
1. Call find_path("Terminal") -> ["Mike", "Bravo", "Delta-2", "Kilo", "CROSS RUNWAY: 27", "Tango", "Terminal"]
2. Call can_enter_runway("27", "Kilo") -> True
"Snow King, taxi to terminal via Mike, Bravo, Delta-2, Kilo, cross Runway 27, Tango, Terminal. Report clear of Runway 27."

Vehicle you're talking to:
{json.dumps(vehicle.model_dump())}
""".strip()),
        ]

    @gpt_function
    def find_path(self, destination: str) -> TaskResponse:
        """Find a path from the aircraft's current location to the destination.
        If the destination is the active runway, you must give it's actual name (Eg "27L" rather than "active runway").
        destination: The destination (e.g. "Gate 26", "27L", "Service apron")
        """
        if destination.lower() == "active runway":
            destination = self.airport.active_runway
        route = self.airport.FindPath(self.vehicle.current_location, destination)
        return TaskResponse(result=True, response=f"Route: {', '.join(route)}")

    @gpt_function
    def can_enter_runway(self, runway_id: str, from_taxiway: str) -> TaskResponse:
        """Used to check if it is safe for a vehicle to enter or cross a runway. Must be used before giving any clearance to enter or cross a runway.
        runway_id: The runway ID (e.g. 27L)
        from_taxiway: The name of the taxiway the vehicle is entering the runway from (e.g. Bravo 2)
        """
        if self.airport.CanEnterRunway(runway_id):
            return TaskResponse(result=True)
        else:
            return TaskResponse(result=False, response=f"Instruct to hold short at {from_taxiway}")



if __name__ == "__main__":
    airport = Airport(TEST_path_across_active_runway=True, TEST_can_enter_runways=True)
    vehicle = AirportVehicle(callsign="Icelandair 456", current_location="Gate 26", vehicle_type="A320")
    tool = ATCTool(airport, vehicle) #, model="gpt-4")

    async def main() -> None:
        user_input = f"Tower, {vehicle} ready to taxi."
        print("User input:")
        print(user_input)
        print("\nFunction call info:")
        response = await tool.process_message(UserMessage(user_input))
        print("\nFirst Response:")
        print(response)
        response = await tool.process_message(UserMessage("Fix the message so it conforms to all the rules of ATC phraseology."))
        print("\nFixed Response:")
        print(response)

    asyncio.run(main())