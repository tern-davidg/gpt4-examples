import dotenv
from openai import OpenAI
import openai
import os

dotenv.load_dotenv()

client = OpenAI()


def UploadSourceFiles(source_folder):
    uploaded_file_ids = []
    for filename in os.listdir(source_folder):
        if not filename.endswith(".py"):
            continue
        file_path = os.path.join(source_folder, filename)
        with open(file_path, "rb") as f:
            uploaded_file = client.files.create(
                file=(file_path, f),
                purpose="assistants"
            )
            uploaded_file_ids.append(uploaded_file.id)
    return uploaded_file_ids


uploaded_file_ids = []
uploaded_file_ids = UploadSourceFiles("utils")
# uploaded_file_ids = UploadSourceFiles(".")
uploaded_file_ids = uploaded_file_ids[:10]

print(uploaded_file_ids)

assistant = client.beta.assistants.create(
    model="gpt-4-1106-preview",
    instructions="""
You are an expert python programmer in our company. You have been provided with python files relevant to the user's query. Use your knowledge base to best respond to the user's queries about the codebase. Use the provided files/code as much as possible, and reuse existing functions/classes wherever possible.
""",
    file_ids=uploaded_file_ids,
    tools=[
        {
            "type": "retrieval"
        }
        # {
        #     "type": "function",
        #     "function":
        #     {
        #         "name": "getCurrentWeather",
        #         "description": "Get the weather in location",
        #         "parameters": {
        #             "type": "object",
        #             "properties": {
        #             "location": {"type": "string", "description": "The city and state e.g. San Francisco, CA"},
        #             "unit": {"type": "string", "enum": ["c", "f"]}
        #             },
        #             "required": ["location"]
        #         }
        #     }
        # }
    ],
)



import time

def wait_on_run(run, thread):
    while run.status == "queued" or run.status == "in_progress":
        print(run.status)
        run = client.beta.threads.runs.retrieve(
            thread_id=thread.id,
            run_id=run.id,
        )
        time.sleep(0.5)
    return run

user_input = "What other functions would be useful in webcam_util.py"


# file = client.files.create(
#   file=open("test.csv", "rb"),
#   purpose='assistants'
# )
# file_ids = [file.id]
file_ids = []


thread = client.beta.threads.create(
  messages=[
    {
      "role": "user",
      "content": user_input,
      "file_ids": file_ids
    }
  ]
)

run = client.beta.threads.runs.create(
  thread_id=thread.id,
  assistant_id=assistant.id,
)


while True:
    run = wait_on_run(run, thread)

    if run.status == "requires_action":
        tool_outputs = []
        for tool_call in run.required_action.submit_tool_outputs.tool_calls:
            print(tool_call.function.name, tool_call.function.arguments)
            tool_outputs.append({
                "tool_call_id": tool_call.id,
                "output": "11c, sunny"
            })

        run = client.beta.threads.runs.submit_tool_outputs(
            thread_id=thread.id,
            run_id=run.id,
            tool_outputs=tool_outputs
        )
        run = wait_on_run(run, thread)
    elif run.status == "completed":
        run_steps = client.beta.threads.runs.steps.list(
            thread_id=thread.id,
            run_id=run.id
        )
        for run_step in run_steps.data[::-1]:
            print(run_step)
            if run_step.type != "tool_calls":
                continue

            for tool_call in run_step.step_details.tool_calls:
                if tool_call.type == "code_interpreter":
                    print(f"```\n{tool_call.code_interpreter.input}\n```\n")
                    for output in tool_call.code_interpreter.outputs:
                        print("Output:")
                        print(f"```\n{output.logs}\n```")
                        print()

        messages = client.beta.threads.messages.list(thread_id=thread.id)
        for message in messages.data[::-1]:
            print(message.content[0].text.value)
            print()

        break