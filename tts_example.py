import openai
import dotenv

dotenv.load_dotenv()

client = openai.AsyncClient()

async def main():
    response = await client.audio.speech.create(
        model="tts-1",
        voice="echo",
        input="Hello world! This is a streaming test.",
        speed=0.8
    )

    import pyaudio
    from pydub import AudioSegment
    from io import BytesIO

    p = pyaudio.PyAudio()
    stream = p.open(format=pyaudio.paInt16,
                    channels=1,
                    rate=22050,
                    output=True)

    first_chunk = True
    chunks = await response.aiter_bytes(chunk_size=48000)
    async for chunk in chunks:
        if chunk:
            # Decode MP3 chunk to PCM using pydub
            audio = AudioSegment.from_file(BytesIO(chunk), format="mp3")

            # For the first chunk, open the PyAudio stream with appropriate format
            if first_chunk:
                stream = p.open(format=p.get_format_from_width(audio.sample_width),
                                channels=audio.channels,
                                rate=audio.frame_rate,
                                output=True)
                first_chunk = False

            # Play the audio chunk
            stream.write(audio.raw_data)

    # Stop and close the stream
    stream.stop_stream()
    stream.close()

    # Terminate PyAudio
    p.terminate()

import asyncio
asyncio.run(main())