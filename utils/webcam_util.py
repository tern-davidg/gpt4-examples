import cv2
import base64

def capture_webcam_photo():
    """
    Captures an image from the webcam.
    """
    cap = cv2.VideoCapture(0)
    ret, frame = cap.read()
    cap.release()
    return frame if ret else None

from PIL import Image
import numpy as np

def display_image(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    pil_image = Image.fromarray(image)
    pil_image.show()

def image_to_base64(image):
    _, buffer = cv2.imencode('.jpg', image)
    return base64.b64encode(buffer).decode()

def CaptureFromWebcam(display=True):
    """
    Captures an image from the webcam and displays it.
    """
    image = capture_webcam_photo()
    if image is not None:
        if display:
            display_image(image)
        base64_image = image_to_base64(image)
        return base64_image
    else:
        print("Failed to capture image from webcam.")
        return None

if __name__ == "__main__":
    base64_image = CaptureFromWebcam()
    print(base64_image)