from typing import Dict


def UserMessage(content: str) -> Dict[str, str]:
    return {
        "role": "user",
        "content": content
    }

def SystemMessage(content: str) -> Dict[str, str]:
    return {
        "role": "system",
        "content": content
    }