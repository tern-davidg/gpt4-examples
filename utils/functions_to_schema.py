import inspect


def generate_schema(functions):
    schema_list = []

    for func in functions:
        # Getting the signature and docstring of the function
        sig = inspect.signature(func)
        docstring = inspect.getdoc(func)
        params = sig.parameters

        # Building the parameters schema
        parameters_schema = {
            "type": "object",
            "properties": {},
            "required": []
        }

        for name, param in params.items():
            # Parameter schema assumes string type for simplicity
            param_schema = {"type": "string"}
            
            # If parameter has a default value, it's not required
            if param.default is not inspect.Parameter.empty:
                if isinstance(param.default, str) and param.default.lower() in ["celsius", "fahrenheit"]:
                    param_schema["enum"] = ["celsius", "fahrenheit"]
                else:
                    param_schema["default"] = param.default
            else:
                parameters_schema["required"].append(name)
            
            # Add description to the parameter if possible
            # Assuming the description might be the line that contains the parameter name
            # This could be enhanced by parsing the docstring more intelligently
            if docstring:
                for line in docstring.split("\n"):
                    if name in line:
                        param_description = line.strip()
                        param_schema["description"] = param_description

            parameters_schema["properties"][name] = param_schema

        # Create the function schema
        function_schema = {
            "type": "function",
            "function": {
                "name": func.__name__,
                "parameters": parameters_schema
            }
        }

        # Add description to the function if there's a docstring
        if docstring:
            function_schema["function"]["description"] = docstring.split("\n")[0]

        # Add the function schema to the list
        schema_list.append(function_schema)

    # Return the entire schema list
    return schema_list
