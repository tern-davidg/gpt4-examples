from abc import ABC, abstractproperty
import inspect
from typing import Any, List
import openai
from openai.types.chat import ChatCompletionSystemMessageParam, ChatCompletionUserMessageParam, ChatCompletionToolMessageParam, ChatCompletionMessageToolCall, ChatCompletionMessageParam
from pydantic import BaseModel
from utils.functions_to_schema import generate_schema
import json


def ToolMessage(tool_call_id: str, content: Any) -> ChatCompletionToolMessageParam:
    return ChatCompletionToolMessageParam(role="tool", tool_call_id=tool_call_id, content=content)

def UserMessage(content: str) -> ChatCompletionUserMessageParam:
    return ChatCompletionUserMessageParam(role="user", content=content)

def SystemMessage(content: str) -> ChatCompletionSystemMessageParam:
    return ChatCompletionSystemMessageParam(role="system", content=content)


def gpt_function(funcobj):
    funcobj.__isgptfunction__ = True
    return funcobj


class TaskResponse(BaseModel):
    result: bool
    response: str = ""


def is_instance_method(instance, attr_name):
    if attr_name in ["system_messages"]:
        return False
    attr = getattr(instance, attr_name)
    if inspect.ismethod(attr):
        return attr.__self__ is instance
    return False


class GPTTool(ABC):
    def __init__(self, model="gpt-4-1106-preview"):
        self.model = model
        self.client = openai.AsyncOpenAI()
        self.messages: List[ChatCompletionMessageParam] = []
        self.gpt_functions = self.__find_gpt_functions()

    @abstractproperty
    def system_messages(self):
        pass

    def get_tool_schema(self):
        return generate_schema(self.gpt_functions)

    def __find_gpt_functions(self):
        gpt_functions = []
        for attr_name in dir(self):
            if is_instance_method(self, attr_name):
                attr = getattr(self, attr_name)
                if getattr(attr, "__isgptfunction__", False):
                    gpt_functions.append(attr)
        return gpt_functions

    def __call_gpt_func(self, function_name: str, arguments: Any) -> TaskResponse:
        func_dict = {func.__name__: func for func in self.gpt_functions}
        func = func_dict[function_name]
        return func(**arguments)
    
    def handle_tool_calls(self, tool_calls: List[ChatCompletionMessageToolCall]):
        response_messages: List[ChatCompletionToolMessageParam] = []
        for tool_call in tool_calls:
            function_data = tool_call.function
            function_name = function_data.name
            arguments = json.loads(function_data.arguments)
            task_response = self.__call_gpt_func(function_name, arguments)
            result = json.dumps(task_response.model_dump())
            print(f"{function_name}({arguments}) -> {result}")
            response_messages.append(ToolMessage(tool_call.id, result))
        return response_messages
    
    def reset_chat(self):
        self.messages = self.system_messages

    async def process_message(self, user_message: ChatCompletionUserMessageParam):
        self.messages.append(user_message)
        while True:
            response = await self.client.chat.completions.create(
                messages=self.messages,
                tools=self.get_tool_schema(),
                model=self.model
            )

            assistant_message = response.choices[0].message

            self.messages.append(assistant_message)

            if assistant_message.tool_calls:
                results = self.handle_tool_calls(assistant_message.tool_calls)
                for result in results:
                    self.messages.append(result)
            else:   
                return assistant_message.content
