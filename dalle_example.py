import dotenv
from openai import OpenAI
import webbrowser

from vision_example import GetDescriptionOfImage

dotenv.load_dotenv()

client = OpenAI()

def GenerateImage(prompt: str):
    response = client.images.generate(
    model="dall-e-3",
    prompt=prompt,
    size="1024x1024",
    quality="standard",
    n=1,
    )

    image_url = response.data[0].url

    return image_url


def SimpleExample():
    image_url = GenerateImage("a white siamese cat")
    webbrowser.open(image_url)


def FancyExample():
    description = GetDescriptionOfImage(display=False)
    image_url = GenerateImage(description)
    webbrowser.open(image_url)


if __name__ == "__main__":
    # SimpleExample()
    FancyExample()